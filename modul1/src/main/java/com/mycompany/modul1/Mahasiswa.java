
package com.mycompany.modul1;

public class Mahasiswa {
   protected String nim, nama;
   protected Handphone handphone;
   protected Tablet tablet;
   protected Labtop labtop;
 
   public void setNama(String nama){
     this.nama= nama;
 }
 public String getNama(){
     return nama;
 }
 public void setNim(String nim){
     this.nim= nim;
 }
 public String getNim(){
     return nim;
 }
 public void setHandphone(Handphone handphone){
     this.handphone=handphone;
 }
 public Handphone getHandphone(){
     return handphone;
 }
  public void setTablet(Tablet tablet){
     this.tablet=tablet;
 }
 public Tablet getTablet(){
     return tablet;
 }
 public void setLabtop(Labtop labtop){
     this.labtop=labtop;
 }
 public Labtop getLabtop(){
     return labtop;
 }
}
