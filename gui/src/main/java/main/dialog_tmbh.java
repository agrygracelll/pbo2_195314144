/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;
import com.mycompany.gui.Mahasiswa;
import com.mycompany.gui.Penduduk;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;


public class dialog_tmbh extends JDialog implements ActionListener {
    private JTable jtable;
    private JScrollPane scrollPane;
    private Mahasiswa maha;
    private Penduduk pendudukArray;
    private JLabel label_penduduk;
    private JLabel label_title;
    private JLabel label_nim;
    private JLabel label_nama;
    private JLabel label_tglLahir;
    private JTextField text_nim;
    private JTextField text_nama;
    private JTextField text_tglLahir;
    private JTextArea area_nama;
    private JComboBox comBox_penduduk;
    private String column;

    public dialog_tmbh() {
        init();
    }

    public void init() {
        this.setLayout(null);

//        String data1[][] = {{"112", "Agry","Makale 31 Maret 2001"}};
//        String data2[][] = {{maha.getNim(), maha.getNama(), maha.getTemptLahir()}};
//        String data[][] = new String[column.Penduduk][main.anggota.length];
//        for (int i = 0; i < column.Penduduk; i++) {
//            for (int j = 0; j < main.anggota.length; j++) {
//                data[i][j]= main.anggota[i].getNama() +", "+ main.anggota[i].getTemptLahir();
//                
//            }
//            
//        }
        String datax[][] = {{main.anggota[0].getNama(), main.anggota[0].getTemptLahir()},
        {main.anggota[1].getNama(), main.anggota[1].getTemptLahir()},
        {main.anggota[2].getNama(), main.anggota[2].getTemptLahir()}};
        String test = "";
        String tes1 = "";
        for (int i = 0; i < main.anggota.length; i++) {
            test = main.anggota[i].getNama();
            tes1 = main.anggota[i].getTemptLahir();
        }
        String dataA[][] = {{test, tes1}};
        String column[] = {"NIM", "TTL"};
        jtable = new JTable(datax, column);

        jtable.setBounds(30, 40, 200, 300);
        scrollPane = new JScrollPane();
        this.add(jtable);
        this.add(scrollPane);
        label_title = new JLabel("Mahasiswa");
        label_title.setBounds(110, 10, 200, 20);
        this.add(label_title);

        label_nama = new JLabel("Nama\t :");
        label_nama.setBounds(80, 60, 100, 50);
        this.add(label_nama);
        String dat = " ";
        System.out.println(main.anggota.length);
        for (int i = 0; i < main.anggota.length; i++) {
            if (main.anggota[i] != null) {
                dat = dat + " " + main.anggota[i].getNama();
            }

        }
        area_nama = new JTextArea(dat);
        area_nama.setBounds(180, 75, 180, 20);
        this.add(area_nama);
        

        label_nim = new JLabel("NIM:");
        label_nim.setBounds(80, 100, 100, 50);
        this.add(label_nim);
        text_nim = new JTextField(main.anggota[0].getNama());
        text_nim.setBounds(180, 115, 180, 20);
        this.add(text_nim);
        label_tglLahir = new JLabel("Tempat, Tanggal Lahir:");
        label_tglLahir.setBounds(80, 140, 100, 50);
        this.add(label_tglLahir);
        text_tglLahir = new JTextField(main.anggota[0].getTemptLahir());
        text_tglLahir.setBounds(180, 155, 180, 20);
        this.add(text_tglLahir);

        label_penduduk = new JLabel("Penduduk:");
        label_penduduk.setBounds(80, 180, 100, 50);
        this.add(label_penduduk);
        comBox_penduduk = new JComboBox((ComboBoxModel) pendudukArray);
        comBox_penduduk.setBounds(180, 195, 180, 20);
        this.add(comBox_penduduk);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

}
