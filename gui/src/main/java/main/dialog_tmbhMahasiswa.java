/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import com.mycompany.gui.Mahasiswa;

public class dialog_tmbhMahasiswa extends JDialog implements ActionListener  {
    private JLabel label_title;
    private JButton button_tmbh;
    private JLabel label_nama;
    private JLabel label_nim;
    private JLabel label_tglLahir;
    private JTextField textField_nama;
    private JTextField textField_nim;
    private JTextField textField_tglLahir;

public dialog_tmbhMahasiswa(){
init();
}
public void init() {
        this.setLayout(null);
        label_title = new JLabel("Form Tambah Mahasiswa");
        label_title.setBounds(120, 10, 200, 20);
        this.add(label_title);

        label_nama = new JLabel("Nama :");
        label_nama.setBounds(122, 60, 100, 50);
        this.add(label_nama);
        textField_nama = new JTextField();
        textField_nama.setBounds(180, 75, 180, 20);
        this.add(textField_nama);

        label_nim = new JLabel("NIM :");
        label_nim.setBounds(133, 100, 100, 50);
        this.add(label_nim);
        textField_nim = new JTextField();
        textField_nim.setBounds(180, 115, 180, 20);
        this.add(textField_nim);

        label_tglLahir = new JLabel("Tempat, Tanggal Lahir :");
        label_tglLahir.setBounds(20, 155, 200, 20);
        this.add(label_tglLahir);
        textField_tglLahir = new JTextField();
        textField_tglLahir.setBounds(180, 155, 180, 20);
        this.add(textField_tglLahir);

        button_tmbh = new JButton("Tambah");
        button_tmbh.setBounds(150, 200, 100, 30);
        this.add(button_tmbh);
        button_tmbh.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
       
        if (e.getSource() == button_tmbh) {

                Mahasiswa mhs = new Mahasiswa();
                mhs.setNama(textField_nama.getText());
                mhs.setNim(textField_nim.getText());
                mhs.setTemptLahir(textField_tglLahir.getText());
                main.anggota[main.jumlah] = mhs;
                main.jumlah++;
                JOptionPane.showMessageDialog(null, textField_nama.getText() + "\nData Mahasiswa tersimpan");
                this.dispose();
            }




}
}
